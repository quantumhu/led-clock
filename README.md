
## LED Clock

I can't read clocks at my bedside at night without my glasses. I need to bring the clock a feet close to read numbers, or look at my phone, both of which I'd like to not do. I just want to be able to look at the wall and be able to tell the time.

So my first instinct was to make my own clock ;-;

**Materials:**

- ESP32 with MicroPython as the firmware

- 6 LEDs (ideally different colours)

- Resistors for these LEDs (I used 10KΩ and 100KΩ resistors)

- Pushbutton and a resistor for this (I used a 330Ω resistor)

- Wires to wire it all

- This code and an internet connection

    - make sure to edit the WiFi details in the code
    

The clock is based off a *binary clock*. The hours are represented with 4 different colour LEDs that represent 1, 2, 4 and 8 respectively. All combinations of hours can be represented with 4 LEDs as I convert the time to 12-hour time (also easier to add numbers up to 12 vs 24!)


The minutes LEDs don't represent the minutes like a minute hand on a clock does. Instead, the left LED is lit if the time is currently in the first 20 minutes of the hour. The right LED is lit in the last 20 minutes. Both LEDs are lit when it's in the middle 20 minutes. When you're half asleep, you don't need to know the exact time, just approximately whether or not it's closer to 8am (continue sleeping) or 9am (oh no!).


*More info about the wiring can be found in the **schematic.png** picture*