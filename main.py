from machine import Pin
from machine import WDT
import network
import time
import ntptime
import os

wifi_ssid = ""
wifi_pass = ""

# create wifi file if not created
w = None
try:
    w = open("wifi.txt", "r")
    print("wifi file found")
    w.close()
except:
    w = open("wifi.txt", "w")
    print("no wifi file found, creating one")
    w.close()

wlan = network.WLAN(network.STA_IF)
wlan.active(True)

# create offset file if not created
f = None
try:
    f = open("utc_offset.txt", "r")
    print("timezone file found")
    f.close()
except:
    f = open("utc_offset.txt", "w")
    f.write("-5")
    print("no timezone file found, creating one")
    f.close()

f = open("utc_offset.txt", "r")
utc_offset = int(f.read())
f.close()

# set up pins
h1 = Pin(15, Pin.OUT)
h2 = Pin(2, Pin.OUT)
h4 = Pin(0, Pin.OUT)
h8 = Pin(4, Pin.OUT)
m1 = Pin(17, Pin.OUT)
m2 = Pin(16, Pin.OUT)
button = Pin(19, Pin.IN)

# watchdog with timeout of 2 minutes
wdt = WDT(timeout=120000)
wdt.feed()
print("watchdog started")

# start webrepl
webrepl.start(password="mmpppp")

def write_password(ssid, password):
    w = open("wifi.txt", "a")
    w.write(str("\n" + ssid + "|" + password))
    w.close()

def wifi_error_state():

    global h1
    global h2

    # toggle the red and yellow leds alternatingly
    while True:
        set_led(h2, 0)
        set_led(h1, 1)
        time.sleep(1)
        set_led(h2, 1)
        set_led(h1, 0)
        time.sleep(1)

def wifi_connect(MAX_TRIES=2):
    global wlan
    global wifi_ssid
    global wifi_pass

    tries = 0
    
    if not wlan.isconnected():
        print("connecting to wifi")
        wlan.connect(wifi_ssid, wifi_pass)

        while tries <= MAX_TRIES:
            time.sleep(5)
            tries += 1

        if wlan.isconnected():
            print("network config: ", wlan.ifconfig())
        else:
            print("wifi connection failed")

def read_wifi_file(contents):
    global wlan
    global wifi_ssid
    global wifi_pass
    global wdt

    if contents.count("|") > 1:
        for network in reversed(contents):
            # try to connect to each wifi network stored
            wifi_ssid, wifi_pass = network.split("|")
            wifi_connect()
    else:
        wifi_ssid, wifi_pass = contents.split("|")
        wifi_connect()

    if not wlan.isconnected():
        # if still not connected, disable wifi, disable watchdog
        # this makes it so someone can enter in the wifi password
        print("all passwords failed, waiting for user interaction")
        wlan.active(False)
        wdt = None
        wifi_error_state()

def set_led(pin, val):
    if pin.value() != val:
        pin.value(val)

def set_clock():
    global utc_offset

    global h1
    global h2
    global h4
    global h8
    global m1
    global m2

    now = time.localtime()

    # apply utc offset
    hour = now[3] + utc_offset
    if hour < 0:
        hour += 24
        
    # convert 24 hour time to 12-hour time
    # hour 0 is 12 noon/midnight
    hour = hour % 12
    if hour == 0:
        hour = 12

    # binary rep of hour, 4 bits, padded if necessary
    binbin = bin(hour)
    binbin = binbin.split("0b")[1]
    while len(binbin) != 4:
        binbin = "0" + binbin

    # set leds
    set_led(h8, int(binbin[0]))
    set_led(h4, int(binbin[1]))
    set_led(h2, int(binbin[2]))
    set_led(h1, int(binbin[3]))
    
    # divide minutes into 3 segments
    # left LED for lower, right LED for upper
    # both on for in between
    minute = now[4]
    if minute >= 0 and minute <= 19:
        set_led(m1, 1)
        set_led(m2, 0)
    elif minute >= 20 and minute <= 39:
        set_led(m1, 1)
        set_led(m2, 1)
    elif minute >= 40 and minute <= 59:
        set_led(m1, 0)
        set_led(m2, 1)

def sync_time():
    
    wifi_connect()

    ntptime.settime()
    set_clock()

def dst_swap(val):
    global utc_offset
    if utc_offset == -5:
        utc_offset = -4
    else:
        utc_offset = -5
    
    f = open("utc_offset.txt", "w")
    f.write(str(utc_offset))
    f.close()

    sync_time()

def loop():

    global wdt

    hour_counter = 0

    while True:
        print("loop and feed")
        wdt.feed()
        starttime = time.time()

        set_clock()
        hour_counter += 1

        # ntp sync every hour
        if hour_counter >= 60:
            sync_time()
            hour_counter = 0

        # https://stackoverflow.com/questions/474528/what-is-the-best-way-to-repeatedly-execute-a-function-every-x-seconds
        time.sleep(60.0 - ((time.time() - starttime) % 60.0))

# enable button changing for dst
button.irq(trigger=Pin.IRQ_RISING, handler=dst_swap)

w = open("wifi.txt", "r")
read_wifi_file(w.read().strip(" \n"))
w.close()

# sync with ntp
ntptime.host = "pool.ntp.org"
ntptime.settime()

# run!
loop()
print("got here")